#!/usr/bin/env bash
echo "Spring pet Clinic backynd"
set -e

echo "Installing java ..."

sudo apt-get install default-jre -y
sudo cp /tmp/spring-petclinic-rest-2.4.2.jar /opt/spring-petclinic-rest-2.4.2.jar
sudo cp /tmp/petclinic-backend.service /etc/systemd/system/petclinic-backend.service
sudo systemctl daemon-reload
sudo systemctl enable petclinic-backend.service
sudo systemctl start petclinic-backend.service


