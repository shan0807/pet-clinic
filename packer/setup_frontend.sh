#!/usr/bin/env bash
echo "Spring pet Clinic frontend"
set -e

echo "Installing npm ..."

cd /opt && sudo git clone https://github.com/spring-petclinic/spring-petclinic-angular.git
sudo chmod 777 /opt/spring-petclinic-angular/
sudo wget https://nodejs.org/dist/v14.17.3/node-v14.17.3-linux-x64.tar.xz
sudo tar -xvf node-v14.17.3-linux-x64.tar.xz 
sudo mv node-v14.17.3-linux-x64/bin/* /usr/local/bin/
sudo mv node-v14.17.3-linux-x64/lib/node_modules/ /usr/local/lib/
npm --version
node --version
sudo npm install -g @angular/cli@latest
#sed -i '35d' /opt/spring-petclinic-angular/package.json
cd /opt/spring-petclinic-angular/ && sudo npm install
sudo cp /tmp/start_pet_clinic_frontend.sh /bin/start_pet_clinic_frontend.sh
sudo chmod +x /bin/start_pet_clinic_frontend.sh
sudo cp /tmp/petclinic-frontend.service /etc/systemd/system/petclinic-frontend.service
sudo systemctl daemon-reload
sudo systemctl enable petclinic-frontend.service
sudo systemctl start petclinic-frontend.service