#!/usr/bin/env bash
echo "install postgresql db"

set -e

echo "Installing postgres ..."

sudo apt-get install wget ca-certificates -y

sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
sudo wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update -y
sudo apt-get -y install postgresql
sudo mv /tmp/pg_hba.conf /etc/postgresql/13/main/pg_hba.conf
sudo mv /tmp/postgresql.conf /etc/postgresql/13/main/postgresql.conf
sudo systemctl enable postgresql
sudo systemctl start postgresql
#sudo -u postgres psql -c "CREATE DATABASE petclinic WITH OWNER = postgres ENCODING = 'UTF8' TABLESPACE = pg_default CONNECTION LIMIT = -1;"
#sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'petclinic';"