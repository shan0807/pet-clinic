
variable "instance_type" {
  default = "t2.micro"
}

data "template_file" "user_data_frontend" {
  template = file("${path.module}/files/templates/user-data-frontend.tpl")
  vars = {
    backend_loadbalancer_endpoint = aws_lb.lb_backend.dns_name
  }
}

data "template_file" "user_data_db" {
  template = file("${path.module}/files/templates/user-data-db.tpl")
}

data "template_file" "user_data_backend" {
  template = file("${path.module}/files/templates/user-data-backend.tpl")
}

variable "application_name" {
  default = "PET-CLINIC"
}

data "aws_ami" "linux_ami" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-*-x86_64-ebs"]
  }
}

