variable "ami_db" {
  default     = "ami-04646bebd39f64784"
  description = "The latest AMI."
}

variable "ami_backend" {
  default     = "ami-0e162f4b4d96645e1"
  description = "The latest AMI."
}

variable "ami_frontend" {
  default     = "ami-0327e8f0c103d46aa"
  description = "The latest AMI."
}