resource "aws_security_group" "fe_sg" {
  name        = "${var.application_name}-FE-SG"
  description = "Security Group For frontend"
  vpc_id      = module.network.vpc_id
  tags = {
    Name = "${var.application_name}-FE-SG"
  }
  ingress {
   from_port   = 0
    to_port     = 0
    protocol    = "-1"
    security_groups = [ aws_security_group.lb_sg.id ]
  } 
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#######

resource "aws_security_group" "lb_sg" {
  name        = "${var.application_name}-ALB-SG"
  description = "Security Group For load balancer"
  vpc_id      = module.network.vpc_id
  tags = {
    Name = "${var.application_name}-ALB-SG"
  }
  ingress {
   from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "lb_frontend" {
  name            = "${var.application_name}-FRONTEND-LB"
  security_groups = [aws_security_group.lb_sg.id]
  subnets         = [module.network.public_subnet1, module.network.public_subnet2]
  enable_deletion_protection = false
  tags = {
    Name        = "${var.application_name}-FRONTEND-LB"
  }
}

resource "aws_lb_target_group" "tgt_grp_frontend" {
  name     = "${var.application_name}-FE-TGT-GRP"
  port     = 80
  protocol = "HTTP"
  vpc_id   = module.network.vpc_id
  health_check {
    path = "/"
  }
  target_type = "instance"
  tags = {
    Name        = "${var.application_name}-TGT-FRONTEND-GRP"
  }
}

resource "aws_lb_listener" "tg_listner" {
  load_balancer_arn = aws_lb.lb_frontend.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    target_group_arn = aws_lb_target_group.tgt_grp_frontend.arn
    type             = "forward"
  }
}


resource "aws_launch_configuration" "lc_frontend" {
  name_prefix                 = "lc-${lower(var.application_name)}-frontend"
  #image_id                    = data.aws_ami.linux_ami.id
  image_id                    = var.ami_frontend # frontend
  #image_id                    = "ami-04d7999cda0006846" # BACKEND
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.ec2_keypair.key_name
  security_groups             = [ aws_security_group.fe_sg.id ]
  associate_public_ip_address = "true"
  user_data = data.template_file.user_data_frontend.rendered
  lifecycle { create_before_destroy = true }
  iam_instance_profile = aws_iam_instance_profile.profile.name
  ebs_block_device {
      device_name = "/dev/sdb"
      volume_type = "gp2"
      volume_size = 100
      encrypted = true
  }
}

resource "aws_autoscaling_group" "asg_frontend" {
  name                  = "${lower(var.application_name)}-asg-frontend"
  vpc_zone_identifier   = [module.network.private_subnet1, module.network.private_subnet2]
  launch_configuration  = aws_launch_configuration.lc_frontend.name
  min_size              = 2
  max_size              = 4
  desired_capacity      = 2
  protect_from_scale_in = "true"
  target_group_arns = [aws_lb_target_group.tgt_grp_frontend.arn]
  lifecycle { create_before_destroy = true }
  tags = concat(
    [
      {
        "key"                 = "Name"
        "value"               = "${var.application_name}-FRONTEND"
        "propagate_at_launch" = true
      }
    ]
  )
}

# resource "aws_autoscaling_policy" "ecs-autoscaling-policy" {
#   name = "ASGAverageCPUUtilization"
#   adjustment_type = "ChangeInCapacity"
#   policy_type = "TargetTrackingScaling"
#   autoscaling_group_name = aws_autoscaling_group.asg.name
#   target_tracking_configuration {  
#     predefined_metric_specification {
#       predefined_metric_type = "ASGAverageCPUUtilization"  
#     }
#     target_value = 70.0
#   }
# }