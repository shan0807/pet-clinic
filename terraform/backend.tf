resource "aws_security_group" "backend_sg" {
  name        = "${var.application_name}-BACKEND-SG"
  description = "Security Group For backend"
  vpc_id      = module.network.vpc_id
  tags = {
    Name = "${var.application_name}-BACKEND-SG"
  }
  ingress {
   from_port   = 0
    to_port     = 0
    protocol    = "-1"
    security_groups = [ aws_security_group.backend_lb_sg.id ]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "backend_lb_sg" {
  name        = "${var.application_name}-BACKEND-LB-SG"
  description = "Security Group For backend"
  vpc_id      = module.network.vpc_id
  tags = {
    Name = "${var.application_name}-BACKEND-LB-SG"
  }
  ingress {
   from_port    = 0
    to_port     = 0
    protocol    = "-1"
    security_groups = [ aws_security_group.fe_sg.id ]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "lb_backend" {
  name            = "${var.application_name}-BACKEND-LB"
  security_groups = [aws_security_group.backend_lb_sg.id]
  subnets         = [module.network.private_subnet1, module.network.private_subnet2]
  enable_deletion_protection = false
  tags = {
    Name        = "${var.application_name}-BACKEND-LB"
  }
}

resource "aws_lb_target_group" "tgt_grp_backend" {
  name     = "${var.application_name}-BACKEND-TGT-GRP"
  port     = 9966
  protocol = "HTTP"
  vpc_id   = module.network.vpc_id
  health_check {
    path = "/"
  }
  target_type = "instance"
  tags = {
    Name        = "${var.application_name}-BACKEND-TGT-GRP"
  }
}

resource "aws_lb_listener" "tg_listner_backend" {
  load_balancer_arn = aws_lb.lb_backend.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    target_group_arn = aws_lb_target_group.tgt_grp_backend.arn
    type             = "forward"
  }
}


resource "aws_launch_configuration" "lc_backend" {
  name_prefix                 = "lc-${lower(var.application_name)}-backend"
  image_id                    = var.ami_backend # frontend
  #image_id                    = "ami-04d7999cda0006846" # BACKEND
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.ec2_keypair.key_name
  security_groups             = [ aws_security_group.backend_sg.id ]
  associate_public_ip_address = "true"
  user_data = data.template_file.user_data_backend.rendered
  lifecycle { create_before_destroy = true }
  iam_instance_profile = aws_iam_instance_profile.profile.name
  ebs_block_device {
      device_name = "/dev/sdb"
      volume_type = "gp2"
      volume_size = 100
      encrypted = true
  }
}

resource "aws_autoscaling_group" "asg" {
  name                  = "${lower(var.application_name)}-asg"
  vpc_zone_identifier   = [module.network.private_subnet1, module.network.private_subnet2]
  launch_configuration  = aws_launch_configuration.lc_backend.name
  min_size              = 2
  max_size              = 4
  desired_capacity      = 2
  protect_from_scale_in = "true"
  target_group_arns = [aws_lb_target_group.tgt_grp_backend.arn]
  lifecycle { create_before_destroy = true }
  tags = concat(
    [
      {
        "key"                 = "Name"
        "value"               = "${var.application_name}-BACKEND"
        "propagate_at_launch" = true
      }
    ]
  )
}
