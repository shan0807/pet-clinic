output "frontend_lb_name" {
  description = "URL of frontend lb"
  value = aws_lb.lb_frontend.dns_name
}

output "db_private_ip" {
  description = "private ip of the db instance"
  value = aws_instance.db.private_ip
}
output "backend_lb_name" {
  description = "backend lb name"
  value = aws_lb.lb_backend.dns_name
}