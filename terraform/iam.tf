# # ## ASG ##

resource "aws_security_group" "sg" {
  name        = "${var.application_name}-FRONTEND-SG"
  description = "Security Group For FRONTEND of petclinic"
  vpc_id      = module.network.vpc_id
  tags = {
    Name       = "${var.application_name}-FRONTEND-SG"
  }
  ingress {
    description = "Allow 80 port from lb"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups = [ aws_security_group.lb_sg.id ]
  }
  ingress {
    description = "Allow ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_iam_role" "role" {
  name = "${var.application_name}-ROLE"
  description = "IAM role for EC2 instances in ASG"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  tags = {
   Name = "${var.application_name}-ROLE"
  }
}

resource "aws_iam_instance_profile" "profile" {
  name = "${var.application_name}-INSTANCE-PROFILE"
  role = aws_iam_role.role.name
}

resource "aws_iam_role_policy_attachment" "ssm_managed_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  role = aws_iam_role.role.name
}
resource "aws_iam_role_policy_attachment" "ssm_directory_service" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMDirectoryServiceAccess"
  role = aws_iam_role.role.name
}