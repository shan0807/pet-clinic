## Core Infra ##
module "network" {
    source  = "./modules/network"
    application_name = var.application_name
    vpc_cidr = "10.0.0.0/16"
    pub_sub_cidr_1 = "10.0.0.0/24"
    pub_sub_cidr_2 = "10.0.1.0/24"
    pri_sub_cidr_1 = "10.0.50.0/24"
    pri_sub_cidr_2 = "10.0.51.0/24"
}

# ## Security Group for instances ##


# ## Public Key ##
resource "aws_key_pair" "ec2_keypair" {
    key_name   = "petclinic_keypair"
  public_key = file("./files/keypair/petclinic_key.pub")
}