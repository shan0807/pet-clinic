#!/bin/bash
## To change the backend url

sudo sed -i 's/localhost/${backend_loadbalancer_endpoint}/g' /opt/spring-petclinic-angular/src/environments/environment.ts
sudo sed -i 's/localhost/${backend_loadbalancer_endpoint}/g' /opt/spring-petclinic-angular/src/environments/environment.prod.ts
sudo systemctl restart petclinic-frontend.service
