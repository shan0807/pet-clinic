resource "aws_instance" "db" {
  ami           = var.ami_db
  instance_type = "t2.micro"
  key_name = aws_key_pair.ec2_keypair.key_name
  subnet_id = module.network.private_subnet2
  vpc_security_group_ids = [ aws_security_group.db_sg.id ]
  user_data = data.template_file.user_data_db.rendered
  iam_instance_profile = aws_iam_instance_profile.profile.name
  tags = {
    "Name" = "${lower(var.application_name)}-postgresql-db"
  }
}

resource "aws_security_group" "db_sg" {
  name        = "${var.application_name}-DB-SG"
  description = "Security Group For DB"
  vpc_id      = module.network.vpc_id
  tags = {
    Name = "${var.application_name}-DB-SG"
  }
  ingress {
   from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    security_groups = [ aws_security_group.backend_sg.id ]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}