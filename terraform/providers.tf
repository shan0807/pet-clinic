provider "aws" {
  region = "eu-central-1"
}

terraform {
  backend "s3" {
    bucket = "terraform-state-s3-bucket-shantanu"
    key    = "clinic/pet-clinic.tfstate"
    region = "eu-central-1"
  }
}
