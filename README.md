# PETCLINIC
## Installation of pet-clinic using terraform

**Prerequisites:**
```
- AWS Access and Secret keys with required privileges
- Terraform 0.14 or higher
- packer
- AWS CLI
```
## Implementation

 1. Run the following commands to export aws credentials

```bash
# Windows

set AWS_ACCESS_KEY_ID=XXXXXXXXXXXX
set AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXXXXXXXXXX

# macOS and Linux

export AWS_ACCESS_KEY_ID=XXXXXXXXX
export AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXX
```

2. Clone the code from  [here](https://github.com/shan0807/pet-clinic)

3. Please generate a SSH key which will be used to create the key-pair

```bash
ssh-keygen -m PEM -t rsa -b 2048 -f petclinic_key
```
4. Run the following commands to create AMIs using packer for db

```bash
packer_out=$(packer build db.json | tee /dev/tty)
ami=$(echo "$packer_out" | tail -c 30 | perl -n -e'/: (ami-.+)$/ && print $1')
export AMI_GENERATED_BY_PACKER_BACKEND="$ami" && envsubst < ../terraform/ami.tf.template > ../terraform/ami.tf
```
5. Run the following commands to create AMIs using packer for backend

```bash
packer_out=$(packer build backend.json | tee /dev/tty)
ami=$(echo "$packer_out" | tail -c 30 | perl -n -e'/: (ami-.+)$/ && print $1')
export AMI_GENERATED_BY_PACKER_BACKEND="$ami" && envsubst < ../terraform/ami.tf.template > ../terraform/ami.tf
```
6. Run the following commands to create AMIs using packer for frontend

```bash
packer_out=$(packer build frontend.json | tee /dev/tty)
ami=$(echo "$packer_out" | tail -c 30 | perl -n -e'/: (ami-.+)$/ && print $1')
export AMI_GENERATED_BY_PACKER_BACKEND="$ami" && envsubst < ../terraform/ami.tf.template > ../terraform/ami.tf
```
7. Run `Terraform init` for initialization

8. Run `terraform plan` to display all the resources to be created

9.  Run `terraform apply` to create all the resources, pass an argument as `yes` to confirm else run `terraform apply --auto-approve` 

7. Use the `frontend_lb_name` provided by apply command to access the pet-clinic home page for installation

> ![](/terraform/files/images/home.png)

8. You can add owners by filling the fields in the form

>![](/terraform/files/images/owners.png)
